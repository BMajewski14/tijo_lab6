package pl.edu.pwsztar.domain.mapper;


import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CounterDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class CounterMapper {
    public CounterDto mapToDto(long counter){
        CounterDto counterDto = new CounterDto();

        counterDto.setCountNumber(counter);

        return counterDto;
    }
}
