package pl.edu.pwsztar.domain.dto;

public class CounterDto {
    private long countNumber;

    public CounterDto() {

    }

    public long getCountNumber() {
        return countNumber;
    }

    public void setCountNumber(long countNumber) {
        this.countNumber = countNumber;
    }
}
